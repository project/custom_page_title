# Custom Page Title

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

Override Page Title via Check Path Alias. Add new page Title in Configuration Form.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/custom_page_title).
- Submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/custom_page_title).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configure the custom page title at (/admin/config/custom-page-title/custom-page-title-settings).


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
