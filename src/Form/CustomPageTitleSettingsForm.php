<?php

namespace Drupal\custom_page_title\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class of CustomPageTitleSettingsForm.
 */
class CustomPageTitleSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManagerInterface $language_manager = NULL, ModuleHandlerInterface $module_handler) {
    if ($language_manager) {
      $this->languageManager = $language_manager;
    }
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $languageServices = NULL;
    if ($container->has('language_manager')) {
      $languageServices = $container->get('language_manager');
    }
    return new static(
      $languageServices,
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_page_title_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_page_title.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('custom_page_title.settings');
    $form['desc'] = [
      '#type' => 'markup',
      '#markup' => $this->t('
        <b>Enabled:</b> Rule will work only if checkbox is checked.<br>
        <b>Pages:</b> Enter one path per line. Example paths are "/node/1" or "/hello-world" or "/node/*".<br>'),
    ];
    // Headers for table.
    $header = [
      ['data' => $this->t('Enabled'), 'width' => 50],
      ['data' => $this->t('Pages'), 'width' => 400],
      ['data' => $this->t('Page Title'), 'width' => 400],
    ];
    if ($this->languageManager->isMultilingual() || $this->moduleHandler->moduleExists('language')) {
      $header[] = $this->t('Language');
    }
    array_push($header, $this->t('Operation'), $this->t('Weight'));

    // Multi value table form.
    $form['custom_page_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('There are no items yet. Add an item.', []),
      '#prefix' => '<div id="spt-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'custom_page_table-order-weight',
        ],
      ],
    ];

    // Set table values on Add/Remove or on page load.
    $custom_page_table = $form_state->get('custom_page_table');
    if (empty($custom_page_table)) {
      // Set data from configuration on page load.
      // Set empty element if no configurations are set.
      if (NULL !== $config->get('custom_page_table')) {
        $custom_page_table = $config->get('custom_page_table');
        $form_state->set('custom_page_table', $custom_page_table);
      }
      else {
        $custom_page_table = [''];
        $form_state->set('custom_page_table', $custom_page_table);
      }
    }

    // Create row for table.
    foreach ($custom_page_table as $i => $value) {
      $form['custom_page_table'][$i]['#attributes']['class'][] = 'draggable';
      $form['custom_page_table'][$i]['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Status'),
        '#title_display' => 'invisible',
        '#default_value' => $value['status'] ?? '',
      ];
      $form['custom_page_table'][$i]['pages'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Pages'),
        '#title_display' => 'invisible',
        '#required' => TRUE,
        '#cols' => '5',
        '#rows' => '3',
        '#default_value' => $value['pages'] ?? [],
      ];
      $form['custom_page_table'][$i]['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Page Title'),
        '#title_display' => 'invisible',
        '#required' => TRUE,
        '#maxlength' => 60,
        '#attributes' => ['placeholder' => 'Title'],
        '#default_value' => $value['title'] ?? [],
      ];
      // Add Language if site is multilingual.
      if ($this->languageManager->isMultilingual() || $this->moduleHandler->moduleExists('language')) {
        foreach ($this->languageManager->getLanguages() as $langkey => $langvalue) {
          $langNames[$langkey] = $langvalue->getName();
        }
        $form['custom_page_table'][$i]['language'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Language'),
          '#title_display' => 'invisible',
          '#options' => $langNames,
          '#default_value' => $value['language'] ?? [],
        ];
      }
      $form['custom_page_table'][$i]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => "remove-" . $i,
        '#submit' => ['::removeElement'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::removeCallback',
          'wrapper' => 'spt-fieldset-wrapper',
        ],
        '#index_position' => $i,
      ];
      // TableDrag: Weight column element.
      $form['custom_page_table'][$i]['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $value['weight'] ?? [],
        '#attributes' => ['class' => ['custom_page_table-order-weight']],
      ];
    }
    $form['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'spt-fieldset-wrapper',
      ],
    ];

    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['custom_page_table'];
  }

  /**
   * {@inheritdoc}
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $custom_page_table = $form_state->get('custom_page_table');
    array_push($custom_page_table, "");
    $form_state->set('custom_page_table', $custom_page_table);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    return $form['custom_page_table'];
  }

  /**
   * {@inheritdoc}
   */
  public function removeElement(array &$form, FormStateInterface $form_state) {
    // Get table.
    $custom_page_table = $form_state->get('custom_page_table');
    // Get element to remove.
    $remove = key($form_state->getValue('custom_page_table'));
    // Remove element.
    unset($custom_page_table[$remove]);
    // Set an empty element if no elements are left.
    if (empty($custom_page_table)) {
      array_push($custom_page_table, "");
    }
    $form_state->set('custom_page_table', $custom_page_table);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('custom_page_title.settings')->set('custom_page_table', $form_state->getValue('custom_page_table'))->save();
    parent::submitForm($form, $form_state);
  }

}
